import cv2
import cv
import time
import numpy
import base64
import urllib2


class CaptureManager(object):
	def __init__(self, capture, previewWindowManager = None):
		self.previewWindowManager = previewWindowManager

		self._capture = capture
		self._channel = 0
		self._enteredFrame = False
		self._frame = None
		self._state = 0
		self._counter = 0

		# 3 Image variables (Used in detectMotion method)
		self._image0 = None
		self._image1 = None
		self._image2 = None

		# List, will be use to store motion in frames
		self._list = []
		self._list1 = []

		# State variables
		self._drawing = False
		self._mode = True
		self._ix = -1
		self._iy = -1
		self._startCounter = False

		# Roi draw points
		self._roiy = 0
		self._roix = 0
		self._roiyy= 0
		self._roixx = 0

		# List container of mouseEvents
		self._eventList = []

		#List container of threshold values
		self._threshold = []


	@property
	def channel(self):
		return self._channel

	@channel.setter
	def channel(self, value):
		if self._channel != value:
			self._channel = value
			self._frame = None

	@property
	def frame(self):
		"""Returns a frame of the captured image"""
		if self._enteredFrame and self._frame is None:
				_, self._frame = self._capture.retrieve(channel = self._channel)
		return self._frame

	def getFrame(self):
		"""Gets a frame and show it"""
		#Is there is no frame , set entered frame to False.
		if self.frame is None:
			self._enteredFrame = False
			return

		# Draw to the window, if any
		if self.previewWindowManager is not None:
			#Set up the mouseEvents, this is what connects my draw_rect callback
			#to the mouse events.
			self.getMouseEvent()

			# By testing the true value of this variable we make sure that the counter
			# starts only if the roi has been set.
			if self._startCounter is not False:
				self.counter(self.detectMotion())
				cv2.putText(self._frame, "Count: " + str(self._counter), (0, 60), cv2.FONT_HERSHEY_PLAIN, 3, (0, 0, 255))
			# Show each frame of the capture.
			self.previewWindowManager.show(self._frame)

		#Sets frame to None, to get the other frame of the capture
		self._frame = None
		self._enteredFrame = False

	def enterFrame(self):
		"""Capture the next frame, if any """
		# But first, check that any previous frame was exited
		if self._capture is not None:
			self._enteredFrame = self._capture.grab()

	def roi(self, frame):
		"""Returns a Range of interest of a frame """
		#Here we slice the area of interest (Pending) 
		roi = frame[self._roiy:self._roiyy, self._roix:self._roixx]

		if self._drawing is True:
			cv2.rectangle(self._frame, (self._ix, self._iy), (self._roixx,self._roiyy), (0,255,0), 2)
			cv2.rectangle(self._frame, (self._ix+2, self._iy+2), (self._roixx-2, self._roiyy-2), (0, 0, 0), 2)
		return roi

	def detectMotion(self):
		"""Detect motion using absolute difference, and returns the motion in a roi section"""
		#Using a state variable will make sure that this section only runs once, like setup
		if self._state == 0:
			self._image0 = cv2.cvtColor(self.roi(self._frame), cv2.COLOR_RGB2GRAY)
			self._image1 = cv2.cvtColor(self.roi(self._frame), cv2.COLOR_RGB2GRAY)
			self._image2 = cv2.cvtColor(self.roi(self._frame), cv2.COLOR_RGB2GRAY)
		# Setting this variable to 1 makes that the above section only runs once.
		self._state = 1

		self._image0 = self._image1
		self._image1 = self._image2
		self._image2 = cv2.cvtColor(self.roi(self._frame), cv2.COLOR_RGB2GRAY)

		# Gets the difference between 3 images.
		dimg = self.diffImg(self._image0, self._image1, self._image2)
		
		# CountNonZero returns how much has the images change in relation to each other (Motion).
		motion = cv2.countNonZero(dimg)
		cv2.blur(dimg, (5, 5))
		return motion

	def counter(self, motion):
		"""Returns the number of objects counted"""
		#If the motion in the Roi image is greater that the threshold value
		#add the motion value to a list
		if motion > self.getThreshold(motion):
			self._list.append(motion)
			
		
		# If motion is less than threshold , get the len of the list that contains
		# the how many times has been motion in the roi section.

		if motion < self.getThreshold(motion):
			if len(self._list) != 0:
				# This is an aritmetic sum, len of the list minues the len of the list
				# minus 1, this always will return 1.
				Sum = len(self._list) - (len(self._list) - 1)
				# Adds the result of the aritmetic sum(wich is 1) to the counter.
				self._counter += Sum

			#Set the list to [] to start again the counter procces.	
			self._list=[]

		return self._counter
	def getThreshold(self, motion):
		"""Returns the threshold value"""
		# Add all the motion values to the threshold list
		self._threshold.append(motion)
		# Get the maximun value of the threshold list and divide it by 2
		# to get the threshold value 
		threshold = max(self._threshold) / 2
		return threshold

	def diffImg(self, t0, t1, t2):
		"""Uses absdiff to determine the difference between 2 images"""
		# Gets the difference between 2 images
		d1 = cv2.absdiff(t2, t1)
		d2 = cv2.absdiff(t1, t0)
		# Returns the bitwise_and between d1 and d2
  		return cv2.bitwise_and(d1, d2)

  	def getMouseEvent(self):
  		"""Connects the draw_rect method to the mouse callback"""
  		cv2.setMouseCallback(self.previewWindowManager.getWname(), self.draw_rect)
  	
  	# CallBack
  	def draw_rect(self, event, x, y, flags, param):

  		# If mouse left button is pressed, set self._ix, self._iy to the mouse current position
  		if event == 1:
  			self._eventList.append(event)

	  	if event == cv2.EVENT_LBUTTONDOWN:
	  		self._drawing = False
			self._ix, self._iy = x, y

	  	elif event == cv2.EVENT_LBUTTONUP:
	  		#Range of interest starting points
	  		self._roix = self._ix 
	  		self._roiy = self._iy
	  		#Range of interest ending points
	  		self._roixx =  x
	  		self._roiyy = y
	  		self._state = 0
	  		self._startCounter = True
	  		self._drawing = True
	  		


class WindowManager(object):
	def __init__(self, windowName, keypressCallback = None):
		self.keypressCallback = keypressCallback

		self._windowName = windowName
		self._isWindowCreated = False

	@property 
	def isWindowCreated(self):
		return self._isWindowCreated

	def createWindow(self):
		cv2.namedWindow(self._windowName)
		self._isWindowCreated = True

	def show(self, frame):
		cv2.imshow(self._windowName, frame)

	def destroyWindow(self):
		cv2.destroyWindow(self._windowName)
		self._isWindowCreated = False

	def getWname(self):
		return self._windowName


	def processEvents(self):
		keycode = cv2.waitKey(1)
		if self.keypressCallback is not None and keycode != -1:
			#Discard any non-Ascii info encoded by GTK.
			keycode &= 0xFF
			self.keypressCallback(keycode)