import cv2
import time
from managers import *

class Main(object):
	def __init__(self, w_name, source, mirrored=True):
		#Window manager takes 2 arguments <the window name>, <and some event to procces>
		self._windowManager = WindowManager(w_name, self.onKeypress)
		# captureManager takes 2 arguments <The video source or image source> , the windowManager
		self._captureManager= CaptureManager(cv2.VideoCapture("Videos/video1.mp4"), self._windowManager)


	def run(self):
		"""Run the main loop"""
		self._windowManager.createWindow()
		while self._windowManager.isWindowCreated:
			# Let one frame enter
			self._captureManager.enterFrame()
			# Grab the frame entered and show it
			self._captureManager.getFrame()
			# Procces keyboard events (Window managment)
			self._windowManager.processEvents()

			time.sleep(0.02)

	# Keyboard callback
	def onKeypress(self, keycode):
		"""
		Handle a onKeypress

		escape	-> Quit.
		"""
		if keycode == 27: #escape
			self._windowManager.destroyWindow()

	def counter():
		pass

if __name__ == "__main__":
	Main = Main("Main", 0)
	Main.run()
